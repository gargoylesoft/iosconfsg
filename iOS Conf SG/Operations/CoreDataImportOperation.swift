/// Copyright (c) 2020, Gargoyle Software, LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation
import CoreData

enum CoreDataImportError: Error {
    case noInput
    case zeroLengthString
    case stringToDataEncodeFailure
    case jsonDecodeFailure(String)
    case imageDataDecodeFailure
    case coreDataSaveFailure(Error)
}

final class CoreDataImportOperation: Operation {
    private let context: NSManagedObjectContext
    private let inputString: String?
    private let inputData: Data?

    var result: Result<Int, CoreDataImportError>?

    init(context: NSManagedObjectContext, json: String? = nil, data: Data? = nil) {
        inputString = json
        inputData = data
        self.context = context

        super.init()
    }

    private func process(json: String) {
        let trimmed = json.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        guard !trimmed.isEmpty else {
            result = .failure(.zeroLengthString)
            return
        }

        guard let data = trimmed.data(using: .utf8) else {
            result = .failure(.stringToDataEncodeFailure)
            return
        }

        process(data: data)
    }

    private func process(data: Data) {
        guard !isCancelled else { return }

        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd"

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(fmt)

        let records: [PersonDto]
        do {
            records = try decoder.decode([PersonDto].self, from: data)
        } catch {
            result = .failure(.jsonDecodeFailure(error.localizedDescription))
            return
        }

        guard !isCancelled else { return }

        // Unlike no input, getting no records from the server (i.e. an empty array) is not an error!
        guard !records.isEmpty else {
            result = .success(0)
            return
        }

        var numImported = 0

        let backgroundContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        backgroundContext.parent = context

        // We must wait or the Operation will exit before the records are imported.  That's the same
        // reason we can't call performBackgroundTask on our NSPersistentContainer.  Even though we
        // are calling a "wait" type method, we aren't impacting anything but *this* thread and so we
        // haven't impacted the application's performance.
        backgroundContext.performAndWait {
            // This is a convenience for the conference.  I'm simply removing all Core Data
            // records before doing the import to avoid duplication errors when running the
            // app repeatedly.
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Person.fetchRequest()
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            try! backgroundContext.execute(deleteRequest)

            for dto in records {
                guard !isCancelled else {
                    return
                }

                let person = Person(context: backgroundContext)
                person.birthday = dto.birthday
                person.familyName = dto.familyName
                person.givenName = dto.givenName

                if let str = dto.thumbnail {
                    guard let data = Data(base64Encoded: str) else {
                        result = .failure(.imageDataDecodeFailure)
                        return
                    }

                    person.thumbnailBinary = data
                }

                if let str = dto.image {
                    guard let data = Data(base64Encoded: str) else {
                        result = .failure(.imageDataDecodeFailure)
                        return
                    }

                    let picture = Picture(context: backgroundContext)
                    picture.data = data
                    picture.person = person
                }

                numImported += 1
            }

            guard backgroundContext.hasChanges else { return }

            do {
                try backgroundContext.save()
            } catch {
                result = .failure(.coreDataSaveFailure(error))
                return
            }
        }

        if result == nil {
            result = .success(numImported)
        }
    }

    override func main() {
        print("CoreDataOperation started")
        guard !isCancelled else { return }

        if let inputString = inputString {
            self.process(json: inputString)
        } else if let inputData = inputData {
            self.process(data: inputData)
        } else if let str = dependencies.compactMap({ $0 as? StringProvider }).first?.operationOutputString {
            self.process(json: str)
        } else if let data = dependencies.compactMap({ $0 as? DataProvider}).first?.operationOutputData {
            self.process(data: data)
        } else {
            result = .failure(.noInput)
        }
    }
}

extension CoreDataImportOperation: OperationWithResult {
    typealias ResultType = Int
    typealias ErrorType = CoreDataImportError
}
