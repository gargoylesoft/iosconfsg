/// Copyright © 2020 Gargoyle Software, LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation
import Gzip

// Remember that you must add https://github.com/1024jp/GzipSwift to your
// target's Swift Packages list for any of this to work!  Also, the server
// is explicitly *not* adding the Content-Encoding type of gzip because
// iOS automatically decompresses network data with that header, and for
// this demo we want to explicitly decompress it ourselves.

enum GunzipError: Error {
    case failedToDecompress(Error)
    case noInputData
    case notGzipped
}

final class GunzipOperation: Operation {
    private let inputData: Data?

    var result: Result<Data, GunzipError>?

    init(data: Data? = nil) {
        inputData = data
        super.init()
    }

    override func main() {
        print("Unzip operation started")
        guard !isCancelled else { return }

        let data: Data
        
        if let inputData = inputData {
            data = inputData
        } else if let input = dependencies.compactMap({ $0 as? DataProvider }).first?.operationOutputData {
            data = input
        } else {
            result = .failure(.noInputData)
            return
        }

        guard data.isGzipped else {
            result = .failure(.notGzipped)
            return
        }

        guard !isCancelled else { return }

        do {
            result = .success(try data.gunzipped())
        } catch {
            result = .failure(.failedToDecompress(error))
        }
    }
}

extension GunzipOperation: OperationWithResult {
    typealias ResultType = Data
    typealias ErrorType = GunzipError
}

extension GunzipOperation: DataProvider {
    var operationOutputData: Data? {
        guard isFinished, let result = result, isFinished, case let .success(data) = result else { return nil }

        return data
    }
}
