/// Copyright © 2020 Gargoyle Software, LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation

enum DataDownloadError: Error {
    case badUrl
    case downloadFailed(Error)
}

private let api = URL(string: "https://www.gargoylesoft.com/a.php")!

final class DataDownloadOperation: AsyncOperation {
    private var task: URLSessionDataTask?

    var result: Result<Data, DataDownloadError>?

    override func main() {
        print("DataDownload started")
        guard !isCancelled else { return }

        var request = URLRequest(url: api)
        request.addValue("gzip,rot13", forHTTPHeaderField: "Accept-Encoding")

        task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard let self = self, !self.isCancelled else { return }

            defer { self.state = .finished }

            if let error = error {
                self.result = .failure(.downloadFailed(error))
            } else {
                self.result = .success(data!)
            }
        }

        task?.resume()
    }

    override func cancel() {
        task?.cancel()
        super.cancel()
    }
}

extension DataDownloadOperation: OperationWithResult {
    typealias ResultType = Data
    typealias ErrorType = DataDownloadError
}

extension DataDownloadOperation: DataProvider {
    var operationOutputData: Data? {
        guard isFinished, let result = result, case let .success(data) = result else { return nil }

        return data
    }
}
