/// Copyright © 2020 Gargoyle Software, LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import CoreData
import Contacts

extension Person {
    func thumbnail() -> UIImage? {
        guard let data = thumbnailBinary else { return nil }

        return UIImage(data: data)
    }

    func image() -> UIImage? {
        guard let data = picture?.data else { return nil }

        return UIImage(data: data)
    }

    public override func validateForInsert() throws {
        if let givenName = givenName, let familyName = familyName {
            let contact = CNMutableContact()
            contact.givenName = givenName
            contact.familyName = familyName

            let full = CNContactFormatter.string(from: contact, style: .fullName)!
            if fullName == nil || full != fullName {
                fullName = full
            }
        }

        try super.validateForInsert()
    }
}

extension Person: Identifiable {
    public typealias ID = NSManagedObjectID

    public var id: ID {
        get {
            return objectID
        }
    }
}
