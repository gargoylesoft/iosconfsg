/// Copyright © 2020 Gargoyle Software, LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder {
    private let updateFromServerQueue = OperationQueue()

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "iOS_Conf_SG")
        container.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        return container
    }()

    /// Creates a method to be executed at completion of an Operation.
    /// - Parameter op: The operation.
    /// - Parameter msg: The message to display before the success/fail text.
    private func createCompletionBlock<T: OperationWithResult>(for op: T, saying msg: String) -> Void  {
        op.completionBlock = { [unowned self] in
            guard let result = op.result else { return }

            switch result {
            case .failure(let err):
                self.updateFromServerQueue.cancelAllOperations()
                print("\(msg) failed: \(err)")
            case .success:
                print("\(msg) succeeded")
            }
        }
    }

    func saveContext () {
        let context = persistentContainer.viewContext

        guard context.hasChanges else { return }
        do {
            try context.save()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }

    private func updateFromServer() {
        let downloadOperation = DataDownloadOperation()
        createCompletionBlock(for: downloadOperation, saying: "Network download")

        let unzipOperation = GunzipOperation()
        createCompletionBlock(for: unzipOperation, saying: "Decompression")
        unzipOperation.addDependency(downloadOperation)

        let decryptOperation = DecryptOperation()
        createCompletionBlock(for: decryptOperation, saying: "Decryption")
        decryptOperation.addDependency(unzipOperation)

        let coreDataOperation = CoreDataImportOperation(context: persistentContainer.viewContext)
        createCompletionBlock(for: coreDataOperation, saying: "Core Data")
        coreDataOperation.addDependency(decryptOperation)

        updateFromServerQueue.addOperations([downloadOperation, unzipOperation, decryptOperation, coreDataOperation], waitUntilFinished: false)
    }
}

extension AppDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        updateFromServer()

        return true
    }

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
